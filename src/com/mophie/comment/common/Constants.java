package com.mophie.comment.common;

/**
 * system constants
 * 
 * @author hans.chen
 * 
 */
public class Constants {
	
	public static int RESULT_XML = 1;
	public static int RESULT_HTML = 2;
	/**
	 * default charset
	 */
	public static String secret = "mohpie1";
	public static String DEFAULT_ENCODING = "UTF-8";
	public static String DEFAULT_ENCODING_GBK = "gb2312";
	public static final String PARAM_CONTENT_XMLTYPE = "text/xml";
	public static final String PARAM_CONTENT_XML = "<?xml version=\"1.0\" encoding=\"UTF-8\"?>";
	public static final String PARAM_CONTENT_XML_GBK = "<?xml version=\"1.0\" encoding=\"gb2312\"?>";
	public static final String PARAM_COMMON_RETURN = "msg";
	public static String APP_ROOT = "" ;
	
	public static String apnURL = "http://54.223.175.156:8002/sendAPN";
	public static String onlineUrl = "http://52.1.84.29:9090/plugins/presence/status?type=xml&jid=";
	public static String  XMPP_IP = "52.1.84.29" ;
	
	/**
	 * result return
	 */
	String RESULT = "result";
	
	/**
	 * result error
	 */
	String VALIDATE = "error";
	

}
