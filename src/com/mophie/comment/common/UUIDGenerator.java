package com.mophie.comment.common;

import java.util.UUID;
/**
 * @author hans.hchen
 *
 */
public class UUIDGenerator { 
    public UUIDGenerator() { 
    } 
    /** 
     * get uuid
     * @return
     */
    public static String getUUID(){ 
        String s = UUID.randomUUID().toString(); 
        //ȥ�� - ���� 
        return s.substring(0,8)+s.substring(9,13)+s.substring(14,18)+s.substring(19,23)+s.substring(24); 
    } 
    /** 
     * get uuid by number 
     * @param number
     * @return
     */
    public static String[] getUUID(int number){ 
        if(number < 1){ 
            return null; 
        } 
        String[] ss = new String[number]; 
        for(int i=0;i<number;i++){ 
            ss[i] = getUUID(); 
        } 
        return ss; 
    } 
    public static void main(String[] args){ 
        String[] ss = getUUID(10); 
        for(int i=0;i<ss.length;i++){ 
            System.out.println(ss[i]); 
        } 
    } 
}   

