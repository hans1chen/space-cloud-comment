package com.mophie.comment.common;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;

import org.apache.commons.lang.StringUtils;
import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.StatusLine;
import org.apache.http.client.config.RequestConfig;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.impl.conn.PoolingHttpClientConnectionManager;
import org.apache.http.message.BasicNameValuePair;

import com.mophie.comment.dao.NotificationDAO;
import com.mophie.task.PushMessgeThread;

public class HttpUtils {

	public static PushMessgeThread thread;
	public static PushMessgeThread getThread() {
		return thread;
	}

	public static void setThread(PushMessgeThread thread) {
		HttpUtils.thread = thread;
	}

	static CloseableHttpClient client = null;
	static {
		//initialize httpclient parameter
		PoolingHttpClientConnectionManager cm = new PoolingHttpClientConnectionManager();
		cm.setMaxTotal(200);
		cm.setDefaultMaxPerRoute(100);
		client = HttpClients.custom().setConnectionManager(cm).build();


	}
	
	/**
	 * send push request to apn
	 * @param userId
	 * @param userName
	 * @param type
	 * @param message
	 * @throws Exception
	 */
	public static void post(String userId, int type,
			String message) throws Exception {
		HttpPost post = new HttpPost(Constants.apnURL);
		System.out.println("send apv ........username:" + userId  + ",type:" + type+",message:"+message);
		RequestConfig requestConfig = RequestConfig.custom()
				.setSocketTimeout(30000).setConnectTimeout(30000).build();
		post.setConfig(requestConfig);
		post.setHeader("Content-Type",
				"application/x-www-form-urlencoded;charset=UTF-8");
		List<NameValuePair> nvps = new ArrayList<NameValuePair>();
		
		//add user_id and badge_number parameter 
		if (!StringUtils.isEmpty(userId)) {
			nvps.add(new BasicNameValuePair("user_id", userId));
			String[] users = userId.split(",");
			String numbers = "";
			for (int i=0;i<users.length;i++){
				numbers = numbers + users[i] + ":"+NotificationDAO.getNoReadCount(Integer.parseInt(users[i])) +",";
			}
			nvps.add(new BasicNameValuePair("badge_number", numbers));
		}

		
		//add message parameter
		nvps.add(new BasicNameValuePair("message", message));			
		post.setEntity(new UrlEncodedFormEntity(nvps));

		try {
			// execute post operation
			HttpResponse response = client.execute(post);
			StatusLine status = response.getStatusLine();
			if (status.getStatusCode() != 200)
				System.out.println("send push message fail.........." + ",username:" + userId  + ",type:" + type+",message:"+message);

		} finally {
			post.releaseConnection();
			post = null;
		}
	}
	
	/**
	 * check if user is online
	 * @param userName
	 * @return
	 * @throws Exception
	 */
	public static boolean isOnline(String userName) throws Exception {
		if (StringUtils.isEmpty(userName))
			return false;
		
		HttpPost post = new HttpPost(Constants.onlineUrl + userName.trim() + "@" + Constants.XMPP_IP);
		HttpResponse response = client.execute(post);
		HttpEntity entity = response.getEntity();

		//handle result 
		BufferedReader reader = new BufferedReader(new InputStreamReader(
				entity.getContent(), "UTF-8"));
		String line = null;
		while ((line = reader.readLine()) != null) {
			if (line.indexOf("type=\"Unavailable\"") >= 0) {
				return false;
			}
			if (line.indexOf("type=\"error\"") >= 0) {
				return false;
			} else if (line.indexOf("priority") >= 0
					|| line.indexOf("vcard-temp:x:update") >= 0) {
				return true;
			}
		}
		return false;
	}
}
