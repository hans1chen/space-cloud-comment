package com.mophie.comment.common;

import java.io.BufferedReader;
import java.io.ByteArrayOutputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.io.UnsupportedEncodingException;
import java.net.InetAddress;
import java.net.UnknownHostException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.security.SecureRandom;
import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

public class JavaUtils {
	static String ComLibInfo = "";

	static int ComLibVersion = 100;
	 /**
     * Used by the hash method.
     */
    private static Map<String, MessageDigest> digests =
            new ConcurrentHashMap<String, MessageDigest>();
	static String FtoK(long v) {
		double ret_v = (double) v / 1024;
		DecimalFormat numf = new DecimalFormat("0.##");
		return numf.format(ret_v).toString() + "K";
	}

	static String FtoM(long v) {
		double ret_v = (double) v / 1024 / 1024;
		DecimalFormat numf = new DecimalFormat("0.##");
		return numf.format(ret_v).toString() + "M";
	}
	
	public static boolean isWindows() {
		String os = System.getenv("OS");
		if(os == null){
			return false;
		}
		if (os.toLowerCase().indexOf("windows") >= 0) {
			return true;
		}
		return false;
	}
	
	public static String getPassword(int alpha,int number,int specail){
		String password = "";
		String[] specailArray = {"~","!","@","#","$","%","^","*",
				"(",")","{","}"};
		String[] numberArray = {"0","1","2","3","4","5","6","7","8","9"};
		String[] alphaArray = {"a","b","c","d","e","f","g","h","i","j","k",
				"l","m","n","o","p","q","r","s","t","u"
				,"v","w","x","y","z","A","B","C","D","E","F","G","H","I","J","K",
				"L","M","N","O","P","Q","R","S","T","U"
				,"V","W","X","Y","Z"};
		SecureRandom random = new SecureRandom();
		for (int i = 0;i < specail;i ++){
			password = password + specailArray[random.nextInt(specailArray.length)];
		}
		for (int i = 0;i < alpha;i ++){
			password = password + alphaArray[random.nextInt(alphaArray.length)];
		}
		for (int i = 0;i < number;i ++){
			password = password + numberArray[random.nextInt(numberArray.length)];
		}
		return password;
	}
	
	public static String getState() {
		StringBuffer buf = new StringBuffer();
		buf.append("\nThread Count:").append(Thread.activeCount());
		Runtime rt = Runtime.getRuntime();
		buf.append("\navailableProcessors:").append(rt.availableProcessors());
		buf.append("\ntotalMemory:").append(FtoM(rt.totalMemory()));
		buf.append("\nmaxMemory:").append(FtoM(rt.maxMemory()));
		buf.append("\nfreeMemory:").append(FtoM(rt.freeMemory()));
		buf.append("\nUseMemory:").append(
				FtoM(rt.totalMemory() - rt.freeMemory()));
		return buf.toString();
	}
	
	public static String getLocalIp() throws UnknownHostException
	{
		//String host = System.getenv("HOSTNAME");
		//if(host == null){
			InetAddress addres = InetAddress.getLocalHost();
			if(addres == null){
				return "";
			}else{
				return addres.getHostAddress();
			}
		//}else{
		//	return host;
		//}
	}

	public static String getAllThreadInfo() {
		StringBuffer buf = new StringBuffer();
		int count = Thread.activeCount();
		Thread[] trds = new Thread[count];
		Thread.enumerate(trds);
		for (int i = 0; i < trds.length; i++) {
			if (trds[i] == null) {
				buf.append("\n" + i + ":null");
			} else {
				buf.append("\n" + i + ":" + trds[i].getName() + ":"
						+ trds[i].getPriority());
			}

		}
		return buf.toString();
	}

	public static String getStateShort() {
		StringBuffer buf = new StringBuffer();
		Runtime rt = Runtime.getRuntime();
		buf.append(Thread.activeCount()).append(" ");
		buf.append(FtoM(rt.totalMemory())).append(" ");
		buf.append(FtoM(rt.freeMemory())).append(" ").append(
				FtoM(rt.maxMemory()));
		return buf.toString();
	}
	
	
	public static String getDebugString(String str,int stackLevel) {
		try {
			throw new Exception();
		} catch (Exception ex) {
			StackTraceElement[] els = ex.getStackTrace();
			if (els.length >= stackLevel + 1) {
				StackTraceElement el = els[stackLevel];
				StringBuilder buf = new StringBuilder();
				buf.append(sdf.format(new Date())).append(" (");
				buf.append(el.getFileName()).append(":");
				buf.append(el.getLineNumber()).append(")").append(str);
				return buf.toString();
			}
			return "";
		}
	}

	public static String getDebugString(String str) {
		return getDebugString(str,1);
	}

	static SimpleDateFormat sdf = new SimpleDateFormat("MM-dd HH:mm:ss.SSS");

	public static void debugPrint(Object str) {
		try {
			throw new Exception();
		} catch (Exception ex) {
			StackTraceElement[] els = ex.getStackTrace();
			if (els.length >= 2) {
				StackTraceElement el = els[1];
				StringBuilder buf = new StringBuilder();
				buf.append(sdf.format(new Date())).append(" (");
				buf.append(el.getFileName()).append(":");
				buf.append(el.getLineNumber()).append(")").append(str);
				System.out.println(buf.toString());
			}
		}
	}

	public static void printState() {
		System.out.println(getState());
	}

	public static String getComLibInfo() {
		return ComLibInfo;
	}

	public static void printComLibInfo() {
		System.out.println(getComLibInfo());
	}

	public static int getComLibVersion() {
		return ComLibVersion;
	}

	public static int bytesToInt(byte[] data) {
		if (data.length != 4) {
			throw new IllegalArgumentException("need data.length == 4");
		}
		int v = 0;
		int tiv = 255;
		for (int i = 0; i < 4; i++) {
			if (v != 0) {
				v <<= 8;
			}
			int tmp_int = tiv & data[i];
			v |= tmp_int;
		}
		return v;
	}

	public static byte[] intToBytes__bak(int v) {
		try {
			ByteArrayOutputStream baos = new ByteArrayOutputStream();
			DataOutputStream dos = new DataOutputStream(baos);
			dos.writeInt(v);
			dos.close();
			baos.close();
			return baos.toByteArray();
		} catch (IOException ex) {
			throw new RuntimeException(ex.toString());
		}
	}

	public static final int[] DEC = { -1, -1, -1, -1, -1, -1, -1, -1, -1, -1,
			-1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1,
			-1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1,
			-1, -1, -1, -1, 0, 1, 2, 3, 4, 5, 6, 7, 8, 9, -1, -1, -1, -1, -1,
			-1, -1, 10, 11, 12, 13, 14, 15, -1, -1, -1, -1, -1, -1, -1, -1, -1,
			-1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1,
			10, 11, 12, 13, 14, 15, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1,
			-1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1,
			-1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1,
			-1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1,
			-1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1,
			-1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1,
			-1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1,
			-1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1,
			-1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1,
			-1, -1, -1, -1, -1, -1, };

	public static final byte[] HEX = { (byte) '0', (byte) '1', (byte) '2',
			(byte) '3', (byte) '4', (byte) '5', (byte) '6', (byte) '7',
			(byte) '8', (byte) '9', (byte) 'A', (byte) 'B', (byte) 'C',
			(byte) 'D', (byte) 'E', (byte) 'F' };

	public static byte[] hexToBytes(String digits) {
		ByteArrayOutputStream baos = new ByteArrayOutputStream();
		for (int i = 0; i < digits.length(); i += 2) {
			char c1 = digits.charAt(i);
			if ((i + 1) >= digits.length()) {
				throw new IllegalArgumentException();
			}
			char c2 = digits.charAt(i + 1);
			byte b = 0;
			if ((c1 >= '0') && (c1 <= '9')) {
				b += ((c1 - '0') * 16);
			} else if ((c1 >= 'a') && (c1 <= 'f')) {
				b += ((c1 - 'a' + 10) * 16);
			} else if ((c1 >= 'A') && (c1 <= 'F')) {
				b += ((c1 - 'A' + 10) * 16);
			} else {
				throw new IllegalArgumentException();
			}
			if ((c2 >= '0') && (c2 <= '9')) {
				b += (c2 - '0');
			} else if ((c2 >= 'a') && (c2 <= 'f')) {
				b += (c2 - 'a' + 10);
			} else if ((c2 >= 'A') && (c2 <= 'F')) {
				b += (c2 - 'A' + 10);
			} else {
				throw new IllegalArgumentException();
			}
			baos.write(b);
		}
		return (baos.toByteArray());
	}

	public static String bytesToHex(byte bytes[]) {
		StringBuffer sb = new StringBuffer(bytes.length * 2);
		for (int i = 0; i < bytes.length; i++) {
			sb.append(convertDigit((int) (bytes[i] >> 4)));
			sb.append(convertDigit((int) (bytes[i] & 0x0f)));
		}
		return (sb.toString());

	}

	public static int hexToInt(String hex) {
		byte[] data = hex.getBytes();
		if (data.length > 8) {
			throw new IllegalArgumentException("data is too long");
		}
		int v = 0;
		for (int i = 0; i < data.length; i++) {
			int tv = DEC[data[i]];
			if (tv < 0) {
				throw new IllegalArgumentException("data is not hex");
			}
			if (v != 0) {
				v <<= 4;
			}
			v |= tv;
		}
		return v;
	}

	public static byte[] intToBytes(int v) {
		byte[] data = new byte[4];
		int t_v = v >>> 24;
		data[0] = (byte) t_v;
		t_v = v >>> 16;
		t_v &= 255;
		data[1] = (byte) t_v;
		t_v = v >>> 8;
		t_v &= 255;
		data[2] = (byte) t_v;
		t_v = v & 255;
		data[3] = (byte) t_v;
		return data;
	}

	public static int[] byteToOct(byte b) {
		int[] data = new int[3];
		int v = b;
		data[0] = v;
		data[0] &= 7;
		data[1] = v >> 3;
		data[1] &= 7;
		data[2] = v >> 6;
		data[2] &= 3;
		return data;
	}

	private static char convertDigit(int value) {
		value &= 0x0f;
		if (value >= 10) {
			return ((char) (value - 10 + 'a'));
		} else {
			return ((char) (value + '0'));
		}
	}

	public static String getBytesCon(byte[] data) {
		if (data == null) {
			return "";
		}
		StringBuffer strbuf = new StringBuffer();
		for (int i = 0; i < data.length; i++) {
			if (i % 10 == 0) {
				strbuf.append("\n");
			}
			int t = data[i];
			if (t < 0) {
				t = 256 + t;
			}
			strbuf.append(t).append(";");
		}
		return strbuf.toString();
	}

	public static void printBytesCon(byte[] data) {
		System.out.print("data size:" + data.length);
		System.out.print(getBytesCon(data));
	}

	public static String Exec(String str) throws Exception {
		Runtime rt = Runtime.getRuntime();
		Process p = rt.exec(str);
		InputStream os = p.getInputStream();
		InputStreamReader ins_r = new InputStreamReader(os);
		BufferedReader buf_r = new BufferedReader(ins_r);
		StringBuffer buf_str = new StringBuffer();
		while (true) {
			try {
				str = buf_r.readLine();
				if (str == null) {
					break;
				}
				buf_str.append("\r\n");
				buf_str.append(str);
			} catch (Exception ex) {
				buf_str.append("\n").append(ex.toString());
			}
		}
		return buf_str.toString();
	}

	public static byte[] md5(byte[] data) throws NoSuchAlgorithmException {
		java.security.MessageDigest md5 = MessageDigest.getInstance("MD5");
		md5.update(data);
		return md5.digest();
	}

	public static byte[] md5(InputStream ins) throws NoSuchAlgorithmException,
			IOException {
		java.security.MessageDigest md5 = MessageDigest.getInstance("MD5");
		byte[] buf = new byte[1024 * 16];
		while (true) {
			int readLen = ins.read(buf);
			if (readLen == -1) {
				break;
			}
			md5.update(buf, 0, readLen);

		}
		return md5.digest();
	}

	public static String md5Str(InputStream ins)
			throws NoSuchAlgorithmException, IOException {
		return bytesToHex(md5(ins));
	}

	public static String md5Str(byte[] data) throws NoSuchAlgorithmException {
		return bytesToHex(md5(data));
	}

	static String[] checkEncoding = { "Big5", "GBK", "ISO-8859-1", "UTF-8",
			"US-ASCII" };

	static String[] checkOkEncoding = { "Big5", "GBK", "UTF-8" };

	static int getEncodingQUCount(String str, String encoding)
			throws UnsupportedEncodingException {
		byte[] data = str.getBytes(encoding);
		int count = 0;
		if (data != null) {
			for (int i = 0; i < data.length; i++) {
				if (data[i] == 63) {
					count++;
				}
			}
		}
		return count;
	}

	static int getAllEncodingQUCount(String str)
			throws UnsupportedEncodingException {
		int count = 0;
		for (int i = 0; i < checkEncoding.length; i++) {
			count += getEncodingQUCount(str, checkEncoding[i]);
		}
		return count;
	}

	static boolean isChineseOkString(String str)
			throws UnsupportedEncodingException {
		for (int i = 0; i < checkOkEncoding.length; i++) {
			byte[] data = str.getBytes(checkOkEncoding[i]);
			String tmpStr = new String(data, checkOkEncoding[i]);
			// System.out.print(checkOkEncoding[i]);
			// System.out.print(tmpStr);
			// System.out.print(" ");
			// System.out.println(str);
			if (tmpStr.equals(str) == false) {
				return false;
			}
		}
		return true;
	}

	public static String getExceptionInfo(Throwable th, boolean ishtml) {
		try {
			StringWriter sw = new StringWriter();
			PrintWriter pw = new PrintWriter(sw);
			if (ishtml) {
				pw.write("<pre>\n");
			}
			pw.println(th.getMessage());
			th.printStackTrace(pw);
			if (ishtml) {
				pw.write("</pre>\n");
			}
			pw.close();
			sw.close();
			return sw.toString();
		} catch (Exception ex) {
			RuntimeException re = new RuntimeException(ex.getMessage());
			re.setStackTrace(ex.getStackTrace());
			throw re;
		}
	}

	public static boolean isEqual(byte[] data1, byte[] data2) {
		if (data1.length != data2.length) {
			return false;
		}
		for (int i = 0; i < data1.length; i++) {
			if (data1[i] != data2[i]) {
				return false;
			}
		}
		return true;
	}
	
	public static String encodeBase64(byte[] data) {
		try {
			sun.misc.BASE64Encoder EN = new sun.misc.BASE64Encoder();
			String str = EN.encode(data);
			return str;
		} catch (Exception e) {
			return null;
		}
	}

	public static String encodeBase64(String s) {
		try {
			String url = s;
			sun.misc.BASE64Encoder EN = new sun.misc.BASE64Encoder();
			url = EN.encode(url.getBytes("utf-8"));
			return url;
		} catch (Exception e) {
			return null;
		}
	}
	
	public static String decodeBase64(String s,String encoding) {
		String ret;
		try {
			sun.misc.BASE64Decoder DE = new sun.misc.BASE64Decoder();
			byte[] url = DE.decodeBuffer(s);
			ret = new String(url, encoding);
		} catch (Exception e) {
			ret = null;
		}
		return ret;
	}

	public static String decodeBase64(String s) {
		String ret;
		try {
			sun.misc.BASE64Decoder DE = new sun.misc.BASE64Decoder();
			byte[] url = DE.decodeBuffer(s);
			ret = new String(url, "utf-8");
		} catch (Exception e) {
			ret = null;
		}
		return ret;
	}

	public static boolean charToBoolean(char c) {
		return c == 'T';
	}

	public static char booleanToChar(boolean b) {
		if (b) {
			return 'T';
		} else {
			return 'F';
		}
	}
	
	public static void printEnvInfo()
	{
		System.out.println("env info");
		for(String name:System.getenv().keySet())
		{
			System.out.println(name + ":" + System.getenv(name));
		}
	}
	 /**
     * Hashes a byte array using the specified algorithm and returns the result as a
     * String of hexadecimal numbers. This method is synchronized to avoid
     * excessive MessageDigest object creation. If calling this method becomes
     * a bottleneck in your code, you may wish to maintain a pool of
     * MessageDigest objects instead of using this method.
     * <p>
     * A hash is a one-way function -- that is, given an
     * input, an output is easily computed. However, given the output, the
     * input is almost impossible to compute. This is useful for passwords
     * since we can store the hash and a hacker will then have a very hard time
     * determining the original password.
     * </p>
     * <p>In Jive, every time a user logs in, we simply
     * take their plain text password, compute the hash, and compare the
     * generated hash to the stored hash. Since it is almost impossible that
     * two passwords will generate the same hash, we know if the user gave us
     * the correct password or not. The only negative to this system is that
     * password recovery is basically impossible. Therefore, a reset password
     * method is used instead.</p>
     *
     * @param bytes the byte array to compute the hash of.
     * @param algorithm the name of the algorithm requested.
     * @return a hashed version of the passed-in String
     */
    public static String hash(byte[] bytes, String algorithm) {
        synchronized (algorithm.intern()) {
            MessageDigest digest = digests.get(algorithm);
            if (digest == null) {
                try {
                    digest = MessageDigest.getInstance(algorithm);
                    digests.put(algorithm, digest);
                }
                catch (NoSuchAlgorithmException nsae) {                   
                    return null;
                }
            }
            // Now, compute hash.
            digest.update(bytes);
            return encodeHex(digest.digest());
        }
    }
    /**
     * Turns an array of bytes into a String representing each byte as an
     * unsigned hex number.
     * <p>
     * Method by Santeri Paavolainen, Helsinki Finland 1996<br>
     * (c) Santeri Paavolainen, Helsinki Finland 1996<br>
     * Distributed under LGPL.</p>
     *
     * @param bytes an array of bytes to convert to a hex-string
     * @return generated hex string
     */
    public static String encodeHex(byte[] bytes) {
        StringBuilder buf = new StringBuilder(bytes.length * 2);
        int i;

        for (i = 0; i < bytes.length; i++) {
            if (((int)bytes[i] & 0xff) < 0x10) {
                buf.append("0");
            }
            buf.append(Long.toString((int)bytes[i] & 0xff, 16));
        }
        return buf.toString();
    }
}
