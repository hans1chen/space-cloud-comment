package com.mophie.comment.entity;

import java.util.List;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement(name = "comments")
public class CommentEntities {
	List<CommentEntity> commentEntities;
	int count;

	@XmlElement(name = "count")
	public int getCount() {
		return count;
	}

	public void setCount(int count) {
		this.count = count;
	}

	public CommentEntities() {
	}

	public CommentEntities(List<CommentEntity> commentEntities) {
		this.commentEntities = commentEntities;
	}

	@XmlElement(name = "comment")
	public List<CommentEntity> getCommentEntities() {
		return this.commentEntities;
	}

	public void setCommentEntities(List<CommentEntity> commentEntities) {
		this.commentEntities = commentEntities;
	}
}
