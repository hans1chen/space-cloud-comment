package com.mophie.comment.entity;



public class StatisticEntity implements java.io.Serializable{
	/**
	 * 
	 */
	private static final long serialVersionUID = -1440210924025514896L;
	private int type;
	public int getType() {
		return type;
	}
	public void setType(int type) {
		this.type = type;
	}
	private int userId;
	public int getUserId() {
		return userId;
	}
	public void setUserId(int userId) {
		this.userId = userId;
	}
	
	
}