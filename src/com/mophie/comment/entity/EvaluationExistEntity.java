package com.mophie.comment.entity;


import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

@XmlRootElement(name = "evaluation")
@XmlType(propOrder = { "exist"})
public class EvaluationExistEntity {
	private boolean exist;
	
	@XmlElement
	public boolean isExist() {
		return exist;
	}


	public void setExist(boolean exist) {
		this.exist = exist;
	}


	public EvaluationExistEntity() {
	}
	
	
	
}