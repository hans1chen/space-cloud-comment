package com.mophie.comment.entity;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

@XmlRootElement(name = "notification")
@XmlType(propOrder = { "fileId", "friendId","comment","created","type","read","userId","username"})
public class NotificationEntity {
	private String fileId;	
	private int userId;
	private int friendId;	
	private String comment;
	private int type;
	private int read;
	
	@XmlElement
	public int getRead() {
		return read;
	}
	public void setRead(int read) {
		this.read = read;
	}
	private String created;
	private String username;
	
	@XmlElement
	public String getCreated() {
		return created;
	}
	public void setCreated(String created) {
		this.created = created;
	}
	@XmlElement
	public int getType() {
		return type;
	}
	public void setType(int type) {
		this.type = type;
	}
	@XmlElement
	public String getComment() {
		return comment;
	}
	public void setComment(String comment) {
		this.comment = comment;
	}
	@XmlElement
	public String getFileId() {
		return fileId;
	}
	public void setFileId(String fileId) {
		this.fileId = fileId;
	}
	@XmlElement
	public int getUserId() {
		return userId;
	}
	public void setUserId(int userId) {
		this.userId = userId;
	}
	@XmlElement
	public int getFriendId() {
		return friendId;
	}
	public void setFriendId(int friendId) {
		this.friendId = friendId;
	}
	@XmlElement
	public String getUsername() {
		return username;
	}
	public void setUsername(String username) {
		this.username = username;
	}
	
}