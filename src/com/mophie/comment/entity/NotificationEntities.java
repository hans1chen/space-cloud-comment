package com.mophie.comment.entity;

import java.util.List;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement(name = "notifications")
public class NotificationEntities {

	List<NotificationEntity> notificationNotities;
	@XmlElement(name = "notification")
	public List<NotificationEntity> getNotificationNotities() {
		return notificationNotities;
	}

	public void setNotificationNotities(
			List<NotificationEntity> notificationNotities) {
		this.notificationNotities = notificationNotities;
	}

	public NotificationEntities() {
	}


}
