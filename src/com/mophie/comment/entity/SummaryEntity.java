package com.mophie.comment.entity;


import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

@XmlRootElement(name = "summary")
@XmlType(propOrder = { "comment","evaluation"})
public class SummaryEntity {
	private int comment;
	private int evaluation;
	@XmlElement
	public int getComment() {
		return comment;
	}

	public void setComment(int comment) {
		this.comment = comment;
	}
	@XmlElement
	public int getEvaluation() {
		return evaluation;
	}

	public void setEvaluation(int evaluation) {
		this.evaluation = evaluation;
	}

	public SummaryEntity() {
	}
	
}