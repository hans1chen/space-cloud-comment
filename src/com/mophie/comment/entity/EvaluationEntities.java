package com.mophie.comment.entity;

import java.util.List;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement(name = "evaluations")
public class EvaluationEntities {
	List<EvaluationEntity> evaluationEntities;
	
	@XmlElement(name = "evaluation")
	public List<EvaluationEntity> getEvaluationEntities() {
		return evaluationEntities;
	}

	public void setEvaluationEntities(List<EvaluationEntity> evaluationEntities) {
		this.evaluationEntities = evaluationEntities;
	}

	int count;
	@XmlElement(name = "count")
	public int getCount() {
		return count;
	}

	public void setCount(int count) {
		this.count = count;
	}

	public EvaluationEntities() {
	}

	public EvaluationEntities(List<EvaluationEntity> evaluationEntities) {
		this.evaluationEntities = evaluationEntities;
	}


}
