package com.mophie.comment.entity;

public class OffMessageEntity implements java.io.Serializable{
	/**
	 * 
	 */
	private static final long serialVersionUID = 2400342393396049039L;
	
	private String username;
	private long messageID;
	private String creationDate;
	private int messageSize;
	private String stanza;
	public String getUsername() {
		return username;
	}
	public void setUsername(String username) {
		this.username = username;
	}
	public long getMessageID() {
		return messageID;
	}
	public void setMessageID(long messageID) {
		this.messageID = messageID;
	}
	public String getCreationDate() {
		return creationDate;
	}
	public void setCreationDate(String creationDate) {
		this.creationDate = creationDate;
	}
	public int getMessageSize() {
		return messageSize;
	}
	public void setMessageSize(int messageSize) {
		this.messageSize = messageSize;
	}
	public String getStanza() {
		return stanza;
	}
	public void setStanza(String stanza) {
		this.stanza = stanza;
	}

}
