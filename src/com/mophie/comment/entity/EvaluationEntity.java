package com.mophie.comment.entity;


import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

@XmlRootElement(name = "evaluation")
@XmlType(propOrder = { "fileId", "evaluationId","userId", "created"})
public class EvaluationEntity {
	private String fileId;
	private int userId;
	private int evaluationId;
	private String created;
	public EvaluationEntity() {
	}
	
	@XmlElement
	public String getFileId() {
		return fileId;
	}

	public void setFileId(String fileId) {
		this.fileId = fileId;
	}


	
	@XmlElement
	public int getUserId() {
		return userId;
	}

	public void setUserId(int userId) {
		this.userId = userId;
	}
	@XmlElement
	public int getEvaluationId() {
		return evaluationId;
	}

	public void setEvaluationId(int evaluationId) {
		this.evaluationId = evaluationId;
	}
	@XmlElement
	public String getCreated() {
		return created;
	}

	public void setCreated(String created) {
		this.created = created;
	}	
}