package com.mophie.comment.servlet;

import javax.ws.rs.core.Response;
import javax.ws.rs.ext.ExceptionMapper;
import javax.ws.rs.ext.Provider;

import com.mophie.comment.exception.ErrorResponse;
import com.mophie.comment.exception.ServiceException;

@Provider
public class RESTExceptionMapper
  implements ExceptionMapper<ServiceException>
{
  public Response toResponse(ServiceException exception)
  {
    ErrorResponse errorResponse = new ErrorResponse();
    errorResponse.setRessource(exception.getRessource());
    errorResponse.setMessage(exception.getMessage());
    errorResponse.setException(exception.getException());
    return Response.status(Response.Status.NOT_FOUND).entity(errorResponse).type("application/xml").build();
  }
}