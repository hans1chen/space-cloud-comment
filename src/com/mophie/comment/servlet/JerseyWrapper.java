package com.mophie.comment.servlet;

import java.util.HashMap;
import java.util.Map;

import javax.servlet.ServletConfig;
import javax.servlet.ServletException;

import com.mophie.comment.service.CommentService;
import com.mophie.comment.service.EvaluationService;
import com.mophie.comment.service.NotificationService;
import com.mophie.comment.service.SummaryService;
import com.sun.jersey.api.core.PackagesResourceConfig;
import com.sun.jersey.spi.container.servlet.ServletContainer;


public class JerseyWrapper extends ServletContainer
{
  private static final long serialVersionUID = 1L;
  private static final String SCAN_PACKAGE_DEFAULT = JerseyWrapper.class.getPackage().getName();
  private static Map<String, Object> config = new HashMap<String, Object>();
  private static PackagesResourceConfig prc;

  public JerseyWrapper()
  {
    super(prc);
  }

  public void init(ServletConfig servletConfig)
    throws ServletException
  {
    super.init(servletConfig);

  }

  public void destroy()
  {
    super.destroy();

  }

  static
  {
    config.put("com.sun.jersey.config.property.resourceConfigClass", "com.sun.jersey.api.core.PackagesResourceConfig");
    prc = new PackagesResourceConfig(new String[] { SCAN_PACKAGE_DEFAULT });
    prc.setPropertiesAndFeatures(config);
    prc.getProperties().put("com.sun.jersey.spi.container.ContainerRequestFilters", "com.mophie.comment.servlet.AuthFilter");

    prc.getClasses().add(CommentService.class);
    prc.getClasses().add(EvaluationService.class);
    prc.getClasses().add(SummaryService.class);
    prc.getClasses().add(NotificationService.class);
    prc.getClasses().add(RESTExceptionMapper.class);
  }
}