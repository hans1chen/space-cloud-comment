package com.mophie.comment.servlet;

import java.io.IOException;
import java.util.Timer;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.mophie.comment.common.HttpUtils;
import com.mophie.task.PushMessgeThread;


@WebServlet("/message/notify")
public class NotifyServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public NotifyServlet() {
        super();
    }
    public void init(){
    	Timer timer =  new Timer();
//		timer.schedule(new OffLineTask(),  3 * 1000,  20 * 1000  );
//		
//		timer.schedule(new NotifyTask(),  3 * 1000,  60 * 1000 * 60 * 24);
    	PushMessgeThread pushMessgeThread = new PushMessgeThread();
    	pushMessgeThread.start();
    	HttpUtils.setThread(pushMessgeThread);
    }
	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
	}
	
	
	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

	}

}
