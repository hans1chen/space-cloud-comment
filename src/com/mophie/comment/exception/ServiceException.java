package com.mophie.comment.exception;

public class ServiceException extends Exception
{
  private static final long serialVersionUID = 4351720088030656859L;
  private String ressource;
  private String exception;

  public ServiceException(String msg, String ressource, String exception)
  {
    super(msg);
    this.ressource = ressource;
    this.exception = exception;
  }

  public ServiceException(String msg, Throwable cause)
  {
    super(msg, cause);
  }

  public String getRessource()
  {
    return this.ressource;
  }

  public void setRessource(String ressource)
  {
    this.ressource = ressource;
  }

  public String getException()
  {
    return this.exception;
  }

  public void setException(String exception)
  {
    this.exception = exception;
  }
}