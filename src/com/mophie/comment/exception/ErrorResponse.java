package com.mophie.comment.exception;

import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement(name="error")
public class ErrorResponse
{
  private String ressource;
  private String message;
  private String exception;
  private String exceptionStack;

  public String getRessource()
  {
    return this.ressource;
  }

  public void setRessource(String ressource) {
    this.ressource = ressource;
  }

  public String getMessage() {
    return this.message;
  }

  public void setMessage(String message) {
    this.message = message;
  }

  public String getException() {
    return this.exception;
  }

  public void setException(String exception) {
    this.exception = exception;
  }

  public String getExceptionStack() {
    return this.exceptionStack;
  }

  public void setExceptionStack(String exceptionStack) {
    this.exceptionStack = exceptionStack;
  }
}