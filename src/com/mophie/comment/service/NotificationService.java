package com.mophie.comment.service;

import javax.ws.rs.DELETE;
import javax.ws.rs.DefaultValue;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.Response;
import com.mophie.comment.dao.NotificationDAO;
import com.mophie.comment.entity.NotificationEntities;
import com.mophie.comment.exception.ServiceException;

@Path("notify")
public class NotificationService {

	/**
	 * get unread notify count of user
	 * @param userId
	 * @return
	 * @throws ServiceException
	 */
	@GET
	@Path("/count/{userId}")
	@Produces({ "application/json" })
	public String getNoReadCountByType(@PathParam("userId") int userId)
			throws ServiceException {
			
		String jsonResult = NotificationDAO.getNoReadCountByType(userId);			
		return jsonResult;

	}
	
	/**
	 * get all notifications of user by parameter
	 * @param userId
	 * @param type
	 * @param read
	 * @param offset
	 * @param limit
	 * @return
	 * @throws ServiceException
	 */
	@GET
	@Path("/{userId}/{type}/{read}")
	@Produces({ "application/json" })
	public NotificationEntities getNotifications(@PathParam("userId") int userId,@PathParam("type") String type,@PathParam("read") int read,@DefaultValue("0") @QueryParam("offset") int offset,
			@DefaultValue("50") @QueryParam("limit") int limit)
			throws ServiceException {
			
		NotificationEntities entities = NotificationDAO.getNotifications(userId,type,read,offset,limit);	
		return entities;
	}

	/**
	 * update notification status to read 
	 * @param userId
	 * @param type
	 * @return
	 * @throws ServiceException
	 */
	@PUT
	@Path("/{userId}/{type}")
	public Response UpdateNotification(@PathParam("userId") int userId,@PathParam("type") String type)
			throws ServiceException {
		NotificationDAO.updateNotification(userId,type);
		return Response.status(Response.Status.OK).build();
	}

	@POST
	public Response createNotificaitons(NotificationEntities notificationEntities)
			throws ServiceException {

		NotificationDAO.createNotifications(notificationEntities);
		return Response.status(Response.Status.CREATED).build();
	}
	
	@DELETE
	@Path("/{fileIds}")
	public Response deleteNotification(@PathParam("fileIds") String fileIds)
			throws ServiceException {
		NotificationDAO.deleteNotification(fileIds);
		return Response.status(Response.Status.OK).build();
	}
}