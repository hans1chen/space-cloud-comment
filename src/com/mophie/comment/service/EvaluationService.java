package com.mophie.comment.service;

import java.util.Map;

import javax.ws.rs.DELETE;
import com.google.gson.JsonObject;

import javax.ws.rs.DefaultValue;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.Response;

import com.mophie.comment.dao.EvaluationDAO;
import com.mophie.comment.entity.EvaluationEntities;
import com.mophie.comment.entity.EvaluationEntity;
import com.mophie.comment.exception.ServiceException;

@Path("evaluation")
public class EvaluationService {
	
	@GET
	@Path("/{fileId}")
	@Produces({ "application/json" })
	public EvaluationEntities getEvaluationsJson(@PathParam("fileId") String fileId,@DefaultValue("0") @QueryParam("offset") int offset,
			@DefaultValue("20") @QueryParam("limit") int limit)
			throws ServiceException {
		EvaluationEntities entities = EvaluationDAO.getEvaluations(fileId,offset,limit);		
		return entities;
	}
	
	/**
	 * check if  single file evaluation exist 
	 * @param fileId
	 * @param userId
	 * @return
	 * @throws ServiceException
	 */
	@GET
	@Path("/{fileId}/{userId}")
	@Produces({ "application/json" })
	public String isExistJson(@PathParam("fileId") String fileId,@PathParam("userId") int userId)
			throws ServiceException {
		JsonObject objectTotal = new JsonObject();		
		JsonObject objectResult = new JsonObject();
		boolean isExist = EvaluationDAO.isExist(fileId, userId);		
		objectResult.addProperty("exist", isExist);
		objectTotal.add("data",objectResult);

		return objectTotal.toString();
	}
	
	/**
	 * check if  multiple file evaluation exist 
	 * @param fileIds
	 * @param userId
	 * @return
	 * @throws ServiceException
	 */
	@GET
	@Path("/{fileId}/{userId}/new")
	@Produces({ "application/json" })
	public String isExistsJsonNew(@PathParam("fileId") String fileIds,@PathParam("userId") int userId)
			throws ServiceException {
		JsonObject objectTotal = new JsonObject();		
		Map<String,Integer> isExists = EvaluationDAO.isExists(fileIds, userId);		
		
		String[] fileIdArray = fileIds.split(",");
		for (int i =0;i < fileIdArray.length;i++){		
			objectTotal.addProperty(fileIdArray[i],isExists.get(fileIdArray[i])==null ? false :true);
		}

		return objectTotal.toString();
	}
	
	@DELETE
	@Path("/{fileId}/{userId}")
	public Response deleteEvaluation(@PathParam("fileId") String fileId,@PathParam("userId") int userId)
			throws ServiceException {
		EvaluationDAO.deleteEvaluation(fileId,userId);
		return Response.status(Response.Status.OK).build();
	}

	@POST
	public Response createEvaluation(EvaluationEntity evaluationEntity)
			throws ServiceException {
		//start verify		
		int user_id = evaluationEntity.getUserId();
		if (user_id <= 0)
			throw new ServiceException("evaluation user id should be exist","", "");
		
		boolean isExist = EvaluationDAO.isExist(evaluationEntity.getFileId(), evaluationEntity.getUserId());
		if (isExist)
			throw new ServiceException("evaluation is exist","", "");
		
		//end verify
		EvaluationDAO.createEvaluation(evaluationEntity);
		return Response.status(Response.Status.CREATED).build();
	}
}