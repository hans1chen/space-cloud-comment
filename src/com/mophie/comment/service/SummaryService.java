package com.mophie.comment.service;

import java.util.Map;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;

import com.google.gson.JsonObject;
import com.mophie.comment.dao.CommentDAO;
import com.mophie.comment.dao.EvaluationDAO;
import com.mophie.comment.exception.ServiceException;

@Path("summary")
public class SummaryService {
	
	@GET
	@Path("/{fileId}")
	@Produces({ "application/json" })
	public String getCommentsJson(@PathParam("fileId") String fileId)
			throws ServiceException {
		
		JsonObject objectTotal = new JsonObject();		
		JsonObject objectResult = new JsonObject();		
		
		//get comment count
		int comment = CommentDAO.getTotal(fileId);		
		objectResult.addProperty("comment", comment);
		
		//get evaluation count
		int evaluation = EvaluationDAO.getTotal(fileId);
		objectResult.addProperty("evaluation", evaluation);
		objectTotal.add("data",objectResult);			

		return objectTotal.toString();
	}
	
	@GET
	@Path("/{fileIds}/new")
	@Produces({ "application/json" })
	public String getCommentsJsonNew(@PathParam("fileIds") String fileIds)
			throws ServiceException {
		
		JsonObject objectTotal = new JsonObject();					
		
		Map<String,String> comments = CommentDAO.getTotals(fileIds);		
		Map<String,String> evaluations = EvaluationDAO.getTotals(fileIds);
		String[] fileIdArray = fileIds.split(",");
		for (int i =0;i < fileIdArray.length;i++){
			JsonObject objectResult = new JsonObject();	
			objectResult.addProperty("comment", comments.get(fileIdArray[i])==null ? "0" :comments.get(fileIdArray[i]));
			objectResult.addProperty("evaluation", evaluations.get(fileIdArray[i])==null ? "0" :evaluations.get(fileIdArray[i]));
			objectTotal.add(fileIdArray[i],objectResult);
		}

		return objectTotal.toString();
	}
}