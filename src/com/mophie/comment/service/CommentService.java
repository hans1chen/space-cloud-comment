package com.mophie.comment.service;

import javax.ws.rs.DELETE;
import javax.ws.rs.DefaultValue;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.Response;

import com.mophie.comment.dao.CommentDAO;
import com.mophie.comment.entity.CommentEntities;
import com.mophie.comment.entity.CommentEntity;
import com.mophie.comment.exception.ServiceException;

@Path("comment")
public class CommentService {

	@GET
	@Path("/{fileId}")
	@Produces({ "application/json" })
	public CommentEntities getCommentsJson(@PathParam("fileId") String fileId,@DefaultValue("0") @QueryParam("offset") int offset,
			@DefaultValue("20") @QueryParam("limit") int limit)
			throws ServiceException {
		CommentEntities entities  = CommentDAO.getCommentEntities(fileId,offset,limit);		
		return entities;
	}

	@DELETE
	@Path("/{id}")
	public Response deleteComment(@PathParam("id") int id)
			throws ServiceException {
		CommentDAO.deleteComment(id);
		return Response.status(Response.Status.OK).build();
	}

	@POST
	public Response createComment(CommentEntity commentEntity)
			throws ServiceException {
		//start verify		
		int user_id = commentEntity.getUserId();
		if (user_id <= 0)
			throw new ServiceException("comment user id should be exist","", "");
		
		String comment = commentEntity.getComment();
		if (org.apache.commons.lang.StringUtils.isEmpty(comment.trim()))
			throw new ServiceException("comment can not be null","", "");
		
		//end verify
		CommentDAO.createComment(commentEntity);
		return Response.status(Response.Status.CREATED).build();
	}
}