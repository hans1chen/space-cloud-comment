package com.mophie.comment.dao;

import java.sql.Blob;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.mophie.comment.entity.CommentEntities;
import com.mophie.comment.entity.CommentEntity;
import com.mophie.comment.exception.ServiceException;


/** 
 * comment dao 
 *
 * @author hans.chen
 *
 * @version 1.0
 */
public class CommentDAO {
	private static final String GET_COMMENT =
	        "select a.id,a.user_id,a.file_id,a.created,a.comment  from comments a "
	        + "WHERE a.file_id=? order by a.created desc limit ?,? ";
	
	private static final String GET_COMMENT_COUNT =
	        "select COUNT(1) count from comments WHERE file_id=? ";
	
	private static final String INSERT_COMMENT =
	        "insert into comments(user_id,file_id,created,modified,comment) values(?,?,now(),now(),?)  ";
	
	private static final String DELETE_COMMENT =
	        "delete  from comments  WHERE id=?  ";
	/**
	 * get comment info
	 * @return 
	 * @throws SQLException 
	 */
	public static CommentEntities getCommentEntities(String file_id,int offset,int limit) throws ServiceException {
		Connection connection = null;
        PreparedStatement statement = null;
        ResultSet resultSet = null;
        CommentEntities entities = new CommentEntities();
        int count = getTotal(file_id);
        entities.setCount(count);
        if (count == 0)
        	return entities;
        
        try {
            connection = DbConnectionManager.getConnection();
            statement = connection.prepareStatement(GET_COMMENT);
            statement.setString(1, file_id);
            statement.setLong(2, offset);
            statement.setLong(3, limit);
            resultSet = statement.executeQuery();
            List<CommentEntity> infos = new ArrayList<CommentEntity>();
    		
            while (resultSet.next()) {               
            	CommentEntity info = new CommentEntity();
				info.setCommentId(resultSet.getInt("id"));
				info.setFileId(resultSet.getString("file_id"));
				info.setUserId(resultSet.getInt("user_id"));
				SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
				
				info.setCreated(sdf.format(resultSet.getTimestamp("created")));
				Blob blob = resultSet.getBlob("comment");
				String content = new String(blob.getBytes((long)1, (int)blob.length())); 
				info.setComment(content);
				infos.add(info);
               
            }
            entities.setCommentEntities(infos);
        } catch(Exception e){
        	throw new ServiceException("Could not get comment",file_id+"", e.toString());
        }
        
        finally {
        	 DbConnectionManager.closeConnection(resultSet, statement);
        }

		return entities;
	}
	
	/**
	 * get count
	 * @param file_ids
	 * @return
	 */
	public static Map<String,String> getTotals(String file_ids){
		Connection connection = null;
        PreparedStatement statement = null;
        ResultSet resultSet = null;
        Map<String,String> countMap = new HashMap<String,String>();
        try {
            connection = DbConnectionManager.getConnection();
            String[] typeArray = file_ids.split(",");
			StringBuffer buffer = new StringBuffer(); 
			for (int i = 0; i < typeArray.length; i++) { 
				buffer.append("?, "); 
			} 
			buffer.deleteCharAt(buffer.length() - 1); 
			buffer.deleteCharAt(buffer.length() - 1); 
			
			statement = connection.prepareStatement("select COUNT(1) count,file_id from comments  "
					+ "WHERE   file_id in("+ buffer.toString() +") group by  file_id");
			for (int i = 0; i < typeArray.length; i++) { 
				statement.setString(i + 1,typeArray[i]); 
			} 
            resultSet = statement.executeQuery();
    		
            while (resultSet.next()) {        
            	countMap.put(resultSet.getString("file_id"),resultSet.getString("count"));
            }
            
        } catch(Exception e){
        	e.printStackTrace();
        }
        
        finally {
        	 DbConnectionManager.closeConnection(resultSet, statement);
        }
        return countMap;
	};
	
	/**
	 * get count
	 * @param file_id
	 * @return
	 */
	public static int getTotal(String file_id){
		Connection connection = null;
        PreparedStatement statement = null;
        ResultSet resultSet = null;
        int count = 0;
        try {
            connection = DbConnectionManager.getConnection();
            statement = connection.prepareStatement(GET_COMMENT_COUNT);
            statement.setString(1, file_id);
            resultSet = statement.executeQuery();
    		
            while (resultSet.next()) {                	
				count = resultSet.getInt("count");				               
            }
            
        } catch(Exception e){
        	e.printStackTrace();
        }
        
        finally {
        	 DbConnectionManager.closeConnection(resultSet, statement);
        }
        return count;
	};
	
	/**
	 * create comment
	 * @param commentEntity
	 * @throws ServiceException 
	 */
	public static void createComment(CommentEntity commentEntity) throws ServiceException{
		Connection connection = null;
        PreparedStatement statement = null;
        ResultSet resultSet = null;
        try {
            connection = DbConnectionManager.getConnection();
            statement = connection.prepareStatement(INSERT_COMMENT);
            statement.setLong(1, commentEntity.getUserId());
            statement.setString(2, commentEntity.getFileId());
            statement.setString(3, commentEntity.getComment());
            statement.executeUpdate();

        } catch(Exception e){
        	throw new ServiceException("Could not create comment",commentEntity.getFileId()+"", e.toString());
        }
        
        finally {
        	 DbConnectionManager.closeConnection(resultSet, statement);
        }
	}
	
	/**
	 * delete comment
	 * @param commentId
	 * @throws ServiceException 
	 */
	public static void deleteComment(int commentId) throws ServiceException{
		Connection connection = null;
        PreparedStatement statement = null;
        ResultSet resultSet = null;
        try {
            connection = DbConnectionManager.getConnection();
            statement = connection.prepareStatement(DELETE_COMMENT);
            statement.setLong(1, commentId);
            statement.executeUpdate();

        } catch(Exception e){
        	throw new ServiceException("Could not delete comment",commentId+"", e.toString());
        }
        
        finally {
        	 DbConnectionManager.closeConnection(resultSet, statement);
        }
	}
}
