package com.mophie.comment.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.mophie.comment.entity.EvaluationEntities;
import com.mophie.comment.entity.EvaluationEntity;
import com.mophie.comment.exception.ServiceException;


/** 
 * evaluation dao 
 *
 * @author hans.chen
 *
 * @version 1.0
 */
public class EvaluationDAO {
	private static final String GET_EVALUATION =
	        "select id,user_id,file_id,created from evaluations  WHERE file_id=? order by created desc limit ?,? ";
	
	private static final String GET_EVALUATION_COUNT =
	        "select COUNT(1) count from evaluations WHERE file_id=? ";
	
	private static final String EXIST_EVALUATION =
	        "select id from evaluations WHERE file_id=? AND  user_id=?";
	
	private static final String INSERT_EVALUATION =
	        "insert into evaluations(user_id,file_id,created,modified) values(?,?,now(),now())  ";
	
	private static final String DELETE_EVALUATION =
	        "delete  from evaluations  WHERE file_id=? and user_id=? ";
	/**
	 * get evaluation info
	 * @return 
	 * @throws SQLException 
	 */
	public static EvaluationEntities getEvaluations(String file_id,int offset,int limit) throws ServiceException {
		Connection connection = null;
        PreparedStatement statement = null;
        ResultSet resultSet = null;
        EvaluationEntities entities = new EvaluationEntities();
        int count = getTotal(file_id);
        entities.setCount(count);
        if (count == 0)
        	return entities;
        
        try {
            connection = DbConnectionManager.getConnection();
            statement = connection.prepareStatement(GET_EVALUATION);
            statement.setString(1, file_id);
            statement.setLong(2, offset);
            statement.setLong(3, limit);
            resultSet = statement.executeQuery();
            List<EvaluationEntity> infos = new ArrayList<EvaluationEntity>();
    		
            while (resultSet.next()) {               
            	EvaluationEntity info = new EvaluationEntity();
				info.setEvaluationId(resultSet.getInt("id"));
				info.setFileId(resultSet.getString("file_id"));
				info.setUserId(resultSet.getInt("user_id"));
				SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");				
				info.setCreated(sdf.format(resultSet.getTimestamp("created")));
				infos.add(info);
               
            }
            entities.setEvaluationEntities(infos);
            
        } catch(Exception e){
        	throw new ServiceException("Could not get evaluation",file_id+"", e.toString());
        }
        
        finally {
        	 DbConnectionManager.closeConnection(resultSet, statement);
        }

		return entities;
	}
	
	/**
	 * get multiple file count
	 * @param file_ids
	 * @return
	 */
	public static Map<String,String> getTotals(String file_ids){
		Connection connection = null;
        PreparedStatement statement = null;
        ResultSet resultSet = null;
        Map<String,String> countMap = new HashMap<String,String>();
        try {
            connection = DbConnectionManager.getConnection();
            String[] typeArray = file_ids.split(",");
			StringBuffer buffer = new StringBuffer(); 
			for (int i = 0; i < typeArray.length; i++) { 
				buffer.append("?, "); 
			} 
			buffer.deleteCharAt(buffer.length() - 1); 
			buffer.deleteCharAt(buffer.length() - 1); 
			
			statement = connection.prepareStatement("select COUNT(1) count,file_id from evaluations  "
					+ "WHERE   file_id in("+ buffer.toString() +") group by  file_id");
			for (int i = 0; i < typeArray.length; i++) { 
				statement.setString(i + 1,typeArray[i]); 
			} 
            resultSet = statement.executeQuery();
    		
            while (resultSet.next()) {                	
            	countMap.put(resultSet.getString("file_id"),resultSet.getString("count"));		               
            }
            
        } catch(Exception e){
        	e.printStackTrace();
        }
        
        finally {
        	 DbConnectionManager.closeConnection(resultSet, statement);
        }
        return countMap;
	};
	
	/**
	 * get one file count
	 * @param file_id
	 * @return
	 */
	public static int getTotal(String file_id){
		Connection connection = null;
        PreparedStatement statement = null;
        ResultSet resultSet = null;
        int count = 0;
        try {
            connection = DbConnectionManager.getConnection();
            statement = connection.prepareStatement(GET_EVALUATION_COUNT);
            statement.setString(1, file_id);
            resultSet = statement.executeQuery();
    		
            while (resultSet.next()) {                	
				count = resultSet.getInt("count");				               
            }
            
        } catch(Exception e){
        	e.printStackTrace();
        }
        
        finally {
        	 DbConnectionManager.closeConnection(resultSet, statement);
        }
        return count;
	};
	
	/**
	 * check if evaluation is exist
	 * @param file_id
	 * @param user_id
	 * @return
	 */
	public static boolean isExist(String file_id,int user_id){
		Connection connection = null;
        PreparedStatement statement = null;
        ResultSet resultSet = null;
        boolean exist = false;
        try {
            connection = DbConnectionManager.getConnection();
            statement = connection.prepareStatement(EXIST_EVALUATION);
            statement.setString(1, file_id);
            statement.setLong(2, user_id);
            resultSet = statement.executeQuery();
    		
            while  (resultSet.next()) {                	
            	exist = true;		
            	break;
            }
            
        } catch(Exception e){
        	e.printStackTrace();
        }
        
        finally {
        	 DbConnectionManager.closeConnection(resultSet, statement);
        }
        return exist;
	};
	
	/**
	 * check if multiple files is exist
	 * @param file_ids
	 * @param user_id
	 * @return
	 */
	public static Map<String,Integer> isExists(String file_ids,int user_id){
		Connection connection = null;
        PreparedStatement statement = null;
        ResultSet resultSet = null;
        Map<String,Integer> existMap = new HashMap<String,Integer>();
        try {
            connection = DbConnectionManager.getConnection();
            statement = connection.prepareStatement(EXIST_EVALUATION);
            String[] typeArray = file_ids.split(",");
			StringBuffer buffer = new StringBuffer(); 
			for (int i = 0; i < typeArray.length; i++) { 
				buffer.append("?, "); 
			} 
			buffer.deleteCharAt(buffer.length() - 1); 
			buffer.deleteCharAt(buffer.length() - 1); 
			
			statement = connection.prepareStatement("select file_id from evaluations WHERE file_id in("+ buffer.toString() +") "
					+ " AND  user_id=?");
			for (int i = 0; i < typeArray.length; i++) { 
				statement.setString(i + 1,typeArray[i]); 
			} 
			statement.setLong(typeArray.length + 1, user_id);
            resultSet = statement.executeQuery();
    		
            while  (resultSet.next()) {                	
            	existMap.put(resultSet.getString("file_id"),1);
            }

        } catch(Exception e){
        	e.printStackTrace();
        }
        
        finally {
        	 DbConnectionManager.closeConnection(resultSet, statement);
        }
        return existMap;
	};
	
	/**
	 * create evaluation
	 * @param evaluationEntity
	 * @throws ServiceException 
	 */
	public static void createEvaluation(EvaluationEntity evaluationEntity) throws ServiceException{
		Connection connection = null;
        PreparedStatement statement = null;
        ResultSet resultSet = null;
        try {
            connection = DbConnectionManager.getConnection();
            statement = connection.prepareStatement(INSERT_EVALUATION);
            statement.setLong(1, evaluationEntity.getUserId());
            statement.setString(2, evaluationEntity.getFileId());
 
            statement.executeUpdate();

        } catch(Exception e){
        	throw new ServiceException("Could not create evaluation",evaluationEntity.getFileId()+"", e.toString());
        }
        
        finally {
        	 DbConnectionManager.closeConnection(resultSet, statement);
        }
	}
	
	/**
	 * delete evaluation
	 * @param evaluationId
	 * @throws ServiceException 
	 */
	public static void deleteEvaluation(String fileId,int userId) throws ServiceException{
		Connection connection = null;
        PreparedStatement statement = null;
        ResultSet resultSet = null;
        try {
            connection = DbConnectionManager.getConnection();
            statement = connection.prepareStatement(DELETE_EVALUATION);
            statement.setString(1, fileId);
            statement.setLong(2, userId);
            statement.executeUpdate();

        } catch(Exception e){
        	throw new ServiceException("Could not delete evaluation","",e.toString());
        }
        
        finally {
        	 DbConnectionManager.closeConnection(resultSet, statement);
        }
	}
}
