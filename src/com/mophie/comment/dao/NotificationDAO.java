package com.mophie.comment.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;

import com.google.gson.JsonObject;
import com.mophie.comment.common.HttpUtils;
import com.mophie.comment.entity.NotificationEntities;
import com.mophie.comment.entity.NotificationEntity;
import com.mophie.comment.entity.PushEntity;
import com.mophie.comment.entity.StatisticEntity;
import com.mophie.comment.exception.ServiceException;

/**
 * notification dao
 *
 * @author hans.chen
 *
 * @version 1.0
 */
public class NotificationDAO {


	private static final String GET_NOTIFICATION_COUNT = "select totality,type from statistics WHERE   user_id=? ";
	
	private static final String GET_NOTIFICATION_SUM = "select sum(totality) total from statistics WHERE   user_id=? ";

	private static final String GET_NOTIFICATION_STATUS = "select is_read,user_id,type from notifications WHERE  file_id=?";

	private static final String GET_NOTIFICATION_COUNT_EXIST = "select totality from statistics WHERE   user_id=? and type=?";

	private static final String UPDATE_NOTIFICATION_COUNT_ADD = "update statistics set totality = totality+1  WHERE   user_id=? and type = ?";

	private static final String INSERT_NOTIFICATION_COUNT = "insert into statistics(user_id,type,totality,created,modified) values(?,?,1,now(),now())  ";

	private static final String UPDATE_NOTIFICATION_COUNT_REDUCE = "update statistics set totality = totality-1  WHERE   user_id=? and type = ? and totality>0";

	private static final String INSERT_NOTIFICATION = "insert into notifications(user_id,file_id,friend_id,type,comment,is_read,created,modified,username) values(?,?,?,?,?,?,now(),now(),?)  ";
	
	private static final String DELETE_NOTIFICATION = "delete from notifications  WHERE file_id = ?  ";
	
	private static final String GET_NOTIFICATION_PUSH = "select user_id,type from statistics WHERE  totality >0 ";
	
	/**
	 * get notifications of user by parameter 
	 * @param userId
	 * @param types, 0:all
	 * @param read, 2:all, 1:read ,0:unread
	 * @param offset
	 * @param limit
	 * @return
	 * @throws ServiceException
	 */
	public static NotificationEntities getNotifications(int userId,
			String types,int read, int offset, int limit) throws ServiceException {
		Connection connection = null;
		PreparedStatement statement = null;
		ResultSet resultSet = null;
		NotificationEntities entities = new NotificationEntities();


		try {
			connection = DbConnectionManager.getConnection();
			List<NotificationEntity> infos = new ArrayList<NotificationEntity>();
			if (read == 2) {
				//get all share informations					
				if ("0".equals(types)) {
					//get all types
					statement = connection.prepareStatement("select user_id,type, friend_id,file_id,created,comment,is_read from notifications  "
							+ "WHERE  user_id=?  order by  created desc limit ?,? ");
					
					
					statement.setLong(1, userId);
					statement.setLong(2, offset);
					statement.setLong(3, limit);
					resultSet = statement.executeQuery();
					
		
					handleResultSet(resultSet, infos);
					entities.setNotificationNotities(infos);
					
				} else {
					String[] typeArray = types.split(",");
					StringBuffer buffer = new StringBuffer(); 
					for (int i = 0; i < typeArray.length; i++) { 
						buffer.append("?, "); 
					} 
					buffer.deleteCharAt(buffer.length() - 1); 
					buffer.deleteCharAt(buffer.length() - 1); 
					
					statement = connection.prepareStatement("select user_id,type, friend_id,file_id,created,comment,is_read from notifications  "
							+ "WHERE   type in("+ buffer.toString() +") and user_id=?  order by  created desc limit ?,? ");
					
					for (int i = 0; i < typeArray.length; i++) { 
						statement.setString(i + 1,typeArray[i]); 
					} 
					statement.setLong(typeArray.length + 1, userId);
					statement.setLong(typeArray.length + 2, offset);
					statement.setLong(typeArray.length + 3, limit);
					resultSet = statement.executeQuery();
					
		
					handleResultSet(resultSet, infos);
					entities.setNotificationNotities(infos);
				}
			
			} else {
				if ("0".equals(types)) {
					//get all types
					statement = connection.prepareStatement("select user_id,type, friend_id,file_id,created,comment,is_read from notifications  "
							+ "WHERE  user_id=? and is_read=? order by  created desc limit ?,? ");
					
					
					statement.setLong(1, userId);
					statement.setLong(2, read);
					statement.setLong(3, offset);
					statement.setLong(4, limit);
					resultSet = statement.executeQuery();
					
		
					handleResultSet(resultSet, infos);
					entities.setNotificationNotities(infos);
					
				} else {
					String[] typeArray = types.split(",");
					StringBuffer buffer = new StringBuffer(); 
					for (int i = 0; i < typeArray.length; i++) { 
						buffer.append("?, "); 
					} 
					buffer.deleteCharAt(buffer.length() - 1); 
					buffer.deleteCharAt(buffer.length() - 1); 
					
					statement = connection.prepareStatement("select user_id,type, friend_id,file_id,created,comment,is_read from notifications  "
							+ "WHERE  type in("+ buffer.toString() +") and user_id=? and is_read=? order by  created desc limit ?,? ");
					
					for (int i = 0; i < typeArray.length; i++) { 
						statement.setString(i + 1,typeArray[i]); 
					} 
					statement.setLong(typeArray.length + 1, userId);
					statement.setLong(typeArray.length + 2, read);
					statement.setLong(typeArray.length + 3, offset);
					statement.setLong(typeArray.length + 4, limit);
					resultSet = statement.executeQuery();
					
		
					handleResultSet(resultSet, infos);
					entities.setNotificationNotities(infos);
				}
			}
			
			//if entity is exist, then update
			if (infos.size() > 0 && read != 1) {
				updateNotification(userId,types);
			}
		} catch (Exception e) {
			throw new ServiceException("Could not get notify", userId + "",
					e.toString());
		}

		finally {
			DbConnectionManager.closeConnection(resultSet, statement);
		}

		return entities;
	}
	
	/**
	 * handle result set to entities
	 * @param resultSet
	 * @param infos
	 * @throws SQLException
	 */
	private static void handleResultSet(ResultSet resultSet,
			List<NotificationEntity> infos) throws SQLException {
		while (resultSet.next()) {
			NotificationEntity info = new NotificationEntity();
			info.setFriendId(resultSet.getInt("friend_id"));
			SimpleDateFormat sdf = new SimpleDateFormat(
					"yyyy-MM-dd HH:mm:ss");
			info.setUserId(resultSet.getInt("user_id"));
			info.setFileId(resultSet.getString("file_id"));
			info.setType(resultSet.getInt("type"));
			info.setCreated(sdf.format(resultSet.getTimestamp("created")));
			info.setComment(resultSet.getString("comment"));
			info.setRead(resultSet.getInt("is_read"));
			infos.add(info);

		}
	}

	/**
	 * get noread count json object to service
	 * 
	 * @param user_id
	 * @return
	 */
	public static String getNoReadCountByType(int user_id) {
		Connection connection = null;
		PreparedStatement statement = null;
		ResultSet resultSet = null;
		
		JsonObject objectTotal = new JsonObject();		
		
		try {
			connection = DbConnectionManager.getConnection();
			statement = connection.prepareStatement(GET_NOTIFICATION_COUNT);
			statement.setLong(1, user_id);
			resultSet = statement.executeQuery();
			JsonObject objectResult = new JsonObject();
			while (resultSet.next()) {
				int count = resultSet.getInt("totality");
				int type = resultSet.getInt("type");
				objectResult.addProperty(""+type, ""+count);				
			}
			objectTotal.add("data",objectResult);
		} catch (Exception e) {
			e.printStackTrace();
		}

		finally {
			DbConnectionManager.closeConnection(resultSet, statement);
		}
		return objectTotal.toString();
	};
	
	/**
	 * get noread count to badge_number
	 * 
	 * @param user_id
	 * @return
	 */
	public static int getNoReadCount(int user_id) {
		Connection connection = null;
		PreparedStatement statement = null;
		ResultSet resultSet = null;
		
		int count = 0;	
		
		try {
			connection = DbConnectionManager.getConnection();
			statement = connection.prepareStatement(GET_NOTIFICATION_SUM);
			statement.setLong(1, user_id);
			resultSet = statement.executeQuery();
			while (resultSet.next()) {
				count = resultSet.getInt("total");
				break;			
			}
			
		} catch (Exception e) {
			e.printStackTrace();
		}

		finally {
			DbConnectionManager.closeConnection(resultSet, statement);
		}
		return count;
	};

	/**
	 * create notification
	 * 
	 * @param notificationEntity
	 * @throws ServiceException
	 */
	public static void createNotifications(
			NotificationEntities notificationEntities) throws ServiceException {
		Connection connection = null;
		PreparedStatement statement = null;
		PreparedStatement statementExist = null;
		ResultSet resultSet = null;
		try {
			List<NotificationEntity> notificationList = notificationEntities
					.getNotificationNotities();
			if (notificationList.size() == 0)
				return;

			connection = DbConnectionManager.getConnection();

			statement = connection.prepareStatement(INSERT_NOTIFICATION);
			PushEntity entity = new PushEntity();
			String usernames = "";
			String userIds = "";
			for (int i = 0; i < notificationList.size(); i++) {
				NotificationEntity notificationEntity = notificationList.get(i);
				
				//send push message
				String message = notificationEntity.getComment();
				int type = notificationEntity.getType();
				String username = notificationEntity.getUsername();
				usernames = usernames + username + ",";
				int userId = notificationEntity.getUserId();
				userIds = userIds + userId + ",";
				entity.setMessage(message);
				entity.setType(type);
				
				statement.setLong(1, notificationEntity.getUserId());
				statement.setString(2, notificationEntity.getFileId());
				statement.setLong(3, notificationEntity.getFriendId());
				statement.setLong(4, notificationEntity.getType());
				statement.setString(5, notificationEntity.getComment());
				statement.setLong(6, 0);
				statement.setString(7, notificationEntity.getUsername());
				statement.addBatch();
			}
			entity.setUsernames(usernames);
			entity.setUserIds(userIds);
			HttpUtils.getThread().push(entity);
			
			statement.executeBatch();

			for (int i = 0; i < notificationList.size(); i++) {
				NotificationEntity notificationEntity = notificationList.get(i);

				statementExist = connection
						.prepareStatement(GET_NOTIFICATION_COUNT_EXIST);
				statementExist.setLong(1, notificationEntity.getUserId());
				statementExist.setLong(2, notificationEntity.getType());
				resultSet = statementExist.executeQuery();

				if (resultSet.next()) {
					statement = connection
							.prepareStatement(UPDATE_NOTIFICATION_COUNT_ADD);

				} else {
					statement = connection
							.prepareStatement(INSERT_NOTIFICATION_COUNT);

				}
				statement.setLong(1, notificationEntity.getUserId());
				statement.setLong(2, notificationEntity.getType());
				statement.executeUpdate();
			}

		} catch (Exception e) {
			throw new ServiceException("Could not create notification",
					e.toString(), e.toString());
		}

		finally {
			DbConnectionManager.closeConnection(resultSet, statementExist);
			DbConnectionManager.closeConnection(statement);
		}
	}

	/**
	 * update notification
	 * 
	 * @param entity
	 * @throws ServiceException
	 */
	public static void updateNotification(int userId,String types)
			throws ServiceException {
		Connection connection = null;
		PreparedStatement statementNotify = null;
		PreparedStatement statementRead = null;
		try {
			connection = DbConnectionManager.getConnection();
			if ("0".equals(types)) {				
				
				statementNotify = connection.prepareStatement("update statistics set totality = 0  WHERE   user_id=?  ");				
				statementNotify.setLong( 1, userId);
				statementNotify.executeUpdate();
				
				statementRead = connection.prepareStatement("update notifications set is_read = 1  WHERE   user_id=?  ");				
				statementRead.setLong(1, userId);
				statementRead.executeUpdate();		
				
			} else {
				
				String[] typeArray = types.split(",");
				StringBuffer buffer = new StringBuffer(); 
				for (int i = 0; i < typeArray.length; i++) { 
					buffer.append("?, "); 
				} 
				buffer.deleteCharAt(buffer.length() - 1); 
				buffer.deleteCharAt(buffer.length() - 1); 
				
				statementNotify = connection.prepareStatement("update statistics set totality = 0  WHERE  type in ("+ buffer.toString() +") and user_id=?  ");
				
				for (int i = 0; i < typeArray.length; i++) { 
					statementNotify.setString(i + 1,typeArray[i]); 
				} 
				statementNotify.setLong(typeArray.length + 1, userId);
				statementNotify.executeUpdate();
				
				statementRead = connection.prepareStatement("update notifications set is_read = 1  WHERE  type in ("+ buffer.toString() +") and user_id=?  ");
				
				for (int i = 0; i < typeArray.length; i++) { 
					statementRead.setString(i + 1,typeArray[i]); 
				} 
				statementRead.setLong(typeArray.length + 1, userId);
				statementRead.executeUpdate();	
			}
			

		} catch (Exception e) {
			throw new ServiceException("Could not update notification",
					userId + "", e.toString());
		}

		finally {
			DbConnectionManager.closeStatement(statementNotify);
			DbConnectionManager.closeConnection(statementRead);
		}
	}

	/**
	 * delete notification
	 * 
	 * @param fileId
	 * @throws ServiceException
	 */
	public static void deleteNotification(String fileIds)
			throws ServiceException {
		Connection connection = null;
		PreparedStatement statementStatus = null;
		PreparedStatement statementDelete = null;
		PreparedStatement statementRead = null;
		ResultSet resultSet = null;
		try {
			// first ,check if it has read
			connection = DbConnectionManager.getConnection();
			String[] files = fileIds.split(",");
			for (int i=0;i < files.length;i++) {
				String fileId = files[i].trim();
				int status = 0;
				int notifyUserId = 0;
				int notifyType = 0;
				statementStatus = connection
						.prepareStatement(GET_NOTIFICATION_STATUS);
				statementStatus.setString(1, fileId);
				resultSet = statementStatus.executeQuery();
				while (resultSet.next()) {
					status = resultSet.getInt("is_read");
					notifyUserId = resultSet.getInt("user_id");
					notifyType = resultSet.getInt("type");
					if (status == 1)
						continue;
	
					statementRead = connection
							.prepareStatement(UPDATE_NOTIFICATION_COUNT_REDUCE);
					statementRead.setLong(1, notifyUserId);
					statementRead.setLong(2, notifyType);
					statementRead.executeUpdate();
				}
	
				statementDelete = connection
						.prepareStatement(DELETE_NOTIFICATION);
				statementDelete.setString(1, fileId);
				statementDelete.executeUpdate();
			}

		} catch (Exception e) {
			throw new ServiceException("Could not delete notification", fileIds
					+ "", e.toString());
		}

		finally {			
			DbConnectionManager.closeStatement(statementDelete);
			DbConnectionManager.closeStatement(statementRead);
			DbConnectionManager.closeConnection(resultSet,statementStatus);
		}
	}
	
	
	/**
	 * get statistic infomation to push
	 * 
	 * @return
	 * @throws SQLException
	 */
	public static List<StatisticEntity> getStatistics()  {
		Connection connection = null;
		PreparedStatement statement = null;
		ResultSet resultSet = null;
		List<StatisticEntity> statisticList = new  ArrayList<StatisticEntity>();
		try {
			connection = DbConnectionManager.getConnection();
			statement = connection.prepareStatement(GET_NOTIFICATION_PUSH);
			resultSet = statement.executeQuery();

			while (resultSet.next()) {
				StatisticEntity info = new StatisticEntity();
				info.setType(resultSet.getInt("type"));				
				info.setUserId(resultSet.getInt("user_id"));
				statisticList.add(info);
				
			}
			
		} catch (Exception e) {
			e.printStackTrace();
		}

		finally {
			DbConnectionManager.closeConnection(resultSet, statement);
		}

		return statisticList;
	}
}
