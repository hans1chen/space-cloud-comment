package com.mophie.task;

import java.util.LinkedList;

import com.mophie.comment.common.HttpUtils;
import com.mophie.comment.entity.PushEntity;

public class PushMessgeThread extends Thread {

	private LinkedList<PushEntity> msgQueue = new LinkedList<PushEntity>();

	/**
	 * Insert a new NotificationEntity into the queue.
	 * 
	 * @param theMsg
	 */
	public synchronized void push(PushEntity theMsg) {
		/* place at the end of the queue. */
		msgQueue.addLast(theMsg);
		/* notify any waiters */
		notify();

	}

	/**
	 * Retrieve the first NotificationEntity in the queue.
	 * 
	 * @return the first NotificationEntity in the queue.
	 */
	public synchronized PushEntity popFirst() {
		/* not shutdown, so get the next item from the queue. */
		while (msgQueue.isEmpty()) {
			try {
				wait();
			} catch (InterruptedException e) {
				/* ignore this exception */
			}
		}
		return msgQueue.removeFirst();
	}

	public void run() {
		PushEntity notify = null;
		while (true) {
			try {
				notify = popFirst();
				String message = notify.getMessage();
				String usernames = notify.getUsernames();
				String userIds = notify.getUserIds();
				int type = notify.getType();

				String[] users = usernames.split(",");
				String[] notityUserIds = userIds.split(",");
				String offlineUsers = "";
				
				//get all user of offline by openfire presence API
				for (int i = 0; i < users.length; i++) {
					boolean isOnline = HttpUtils.isOnline(users[i]);
					if (!isOnline) {
						offlineUsers = offlineUsers + notityUserIds[i] + ",";
					}
				}
				
				//send push notifications to all offline users
				if (offlineUsers.length() > 1)
					HttpUtils.post(offlineUsers, type, message);

			} catch (Exception e) {
				e.printStackTrace();
			}
		}

	}
}
