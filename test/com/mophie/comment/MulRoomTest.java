package com.mophie.comment;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

import junit.framework.TestCase;

import org.apache.http.HttpEntity;
import org.apache.http.HttpException;
import org.apache.http.HttpResponse;
import org.apache.http.client.methods.HttpDelete;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.client.methods.HttpPut;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;

import sun.misc.BASE64Encoder;

public class MulRoomTest extends TestCase {

	String postchatUrl = "http://localhost:9090/plugins/restapi/v1/chatrooms";
	String joinchatUrl = "http://54.223.175.156:9090/plugins/restapi/v1/join";
	String messageUrl = "http://52.1.84.29:9090/plugins/restapi/v1/chatrooms/collection_1008,collection_1064/message";
	String addRoleUrl = "http://localhost:9090/plugins/restapi/v1/chatrooms/";
	String getchatUrl = "http://localhost:9090/plugins/restapi/v1/chatrooms/";
	String deletechatUrl = "http://localhost:9090/plugins/restapi/v1/chatrooms/";
	String putChatUrl = "http://localhost:9090/plugins/restapi/v1/chatrooms/";
	CloseableHttpClient client = null;

	protected void setUp() throws Exception {
		super.setUp();
		client = HttpClients.createDefault();
	}

	/**
	 * test post chat
	 * 
	 * @throws HttpException
	 * @throws IOException
	 */
	public void atestPostChat() throws Exception {

		HttpPost post = new HttpPost(postchatUrl);
		String authorization = new BASE64Encoder().encode("admin:admin"
				.getBytes());
		post.setHeader("authorization", authorization);
		post.setHeader("Content-Type", "application/xml;charset=UTF-8");
		HttpEntity entity = new StringEntity(
				"<chatRoom><roomName>aaa</roomName><naturalName>hans-room4</naturalName> <description>Hans Chat Room</description><creationDate>2014-02-12T15:52:37.592+01:00</creationDate>"
						+ "  <maxUsers>0</maxUsers>"
						+ "	 <persistent>true</persistent> <avatar>23</avatar>"
						+ "	 <creationDate>2014-02-12T15:52:37.592+01:00</creationDate>"
						+ "	  <modificationDate>2014-09-12T15:35:54.702+02:00</modificationDate>"
						+ "	 <publicRoom>true</publicRoom><logEnabled>true</logEnabled>"
						+ "	 <registrationEnabled>false</registrationEnabled>"
						+ "	 <canAnyoneDiscoverJID>true</canAnyoneDiscoverJID>"
						+ " <canOccupantsChangeSubject>true</canOccupantsChangeSubject>"
						+ "  <canOccupantsInvite>true</canOccupantsInvite>"
						+ "  <canChangeNickname>true</canChangeNickname>"
						+ " <membersOnly>false</membersOnly>"
						+ "   <moderated>false</moderated>"
						+ "   <broadcastPresenceRoles>"
						+ "      <broadcastPresenceRole>moderator</broadcastPresenceRole>"
						+ "      <broadcastPresenceRole>participant</broadcastPresenceRole>"
						+ "      <broadcastPresenceRole>visitor</broadcastPresenceRole>"
						+ "  </broadcastPresenceRoles>"
						+ "  <members><member>jane@52.1.84.29</member><member>hans@52.1.84.29</member></members>"
					    + " </chatRoom>", "utf-8");

		post.setEntity(entity);

		try {
			long time = System.currentTimeMillis();
			// 执行
			HttpResponse response = client.execute(post);
			HttpEntity entity1 = response.getEntity();
			System.out.println("----------------------------------------");
			System.out.println(response.getStatusLine());
			if (entity1 != null) {
				System.out.println("Response content length: "
						+ entity1.getContentLength());
			}
			BufferedReader reader = new BufferedReader(new InputStreamReader(
					entity1.getContent(), "UTF-8"));
			String line = null;
			while ((line = reader.readLine()) != null) {
				System.out.println(line);
			}

		} finally {

			post.releaseConnection();
			post = null;
		}

	}
	
	/**
	 * test post chat
	 * 
	 * @throws HttpException
	 * @throws IOException
	 */
	public void atestJoin() throws Exception {

		HttpPut get = new HttpPut(joinchatUrl +"?jid=jane");
		System.out.println(joinchatUrl +"?jid=lisa");
		String authorization = new BASE64Encoder().encode("admin:admin123"
				.getBytes());
		get.setHeader("authorization", authorization);
		get.setHeader("Content-Type", "application/xml;charset=UTF-8");
		

		try {
			long time = System.currentTimeMillis();
			// 执行
			HttpResponse response = client.execute(get);
			HttpEntity entity1 = response.getEntity();
			System.out.println("----------------------------------------");
			System.out.println(response.getStatusLine());
			if (entity1 != null) {
				System.out.println("Response content length: "
						+ entity1.getContentLength());
			}
			BufferedReader reader = new BufferedReader(new InputStreamReader(
					entity1.getContent(), "UTF-8"));
			String line = null;
			while ((line = reader.readLine()) != null) {
				System.out.println(line);
			}

		} finally {

			get.releaseConnection();
			get = null;
		}

	}
	
	/**
	 * test post chat
	 * 
	 * @throws HttpException
	 * @throws IOException
	 */
	public void testMessageCount() throws Exception {

		HttpGet get = new HttpGet(messageUrl);
		System.out.println(messageUrl);
		String authorization = new BASE64Encoder().encode("admin:admin123"
				.getBytes());
		get.setHeader("authorization", authorization);
		get.setHeader("Content-Type", "application/xml;charset=UTF-8");
		

		try {
			long time = System.currentTimeMillis();
			// 执行
			HttpResponse response = client.execute(get);
			HttpEntity entity1 = response.getEntity();
			System.out.println("----------------------------------------");
			System.out.println(response.getStatusLine());
			if (entity1 != null) {
				System.out.println("Response content length: "
						+ entity1.getContentLength());
			}
			BufferedReader reader = new BufferedReader(new InputStreamReader(
					entity1.getContent(), "UTF-8"));
			String line = null;
			while ((line = reader.readLine()) != null) {
				System.out.println(line);
			}

		} finally {

			get.releaseConnection();
			get = null;
		}

	}
	
	/**
	 * test post chat
	 * 
	 * @throws HttpException
	 * @throws IOException
	 */
	public void atestLeave() throws Exception {

		HttpDelete get = new HttpDelete(joinchatUrl +"?jid=jane");
		System.out.println(joinchatUrl +"?jid=lisa");
		String authorization = new BASE64Encoder().encode("admin:admin123"
				.getBytes());
		get.setHeader("authorization", authorization);
		get.setHeader("Content-Type", "application/xml;charset=UTF-8");
		

		try {
			long time = System.currentTimeMillis();
			// 执行
			HttpResponse response = client.execute(get);
			HttpEntity entity1 = response.getEntity();
			System.out.println("----------------------------------------");
			System.out.println(response.getStatusLine());
			if (entity1 != null) {
				System.out.println("Response content length: "
						+ entity1.getContentLength());
			}
			BufferedReader reader = new BufferedReader(new InputStreamReader(
					entity1.getContent(), "UTF-8"));
			String line = null;
			while ((line = reader.readLine()) != null) {
				System.out.println(line);
			}

		} finally {

			get.releaseConnection();
			get = null;
		}

	}
	
	/**
	 * test post chatroom
	 * 
	 * @throws HttpException
	 * @throws IOException
	 */
	public void atestAddRole() throws Exception {
		HttpPost post = new HttpPost(addRoleUrl + "aaa/members/fiona");
		System.out.println(addRoleUrl + "cc/members/lisa");
		post.setHeader("Content-Type", "application/xml;charset=UTF-8");
		String authorization = new BASE64Encoder().encode("admin:admin"
				.getBytes());
		post.setHeader("authorization", authorization);

		try {
			long time = System.currentTimeMillis();

			// 执行
			HttpResponse response = client.execute(post);
			HttpEntity entity = response.getEntity();
			System.out.println("----------------------------------------");
			System.out.println(response.getStatusLine());
			if (entity != null) {
				System.out.println("Response content length: "
						+ entity.getContentLength());
			}
			BufferedReader reader = new BufferedReader(new InputStreamReader(
					entity.getContent(), "UTF-8"));
			String line = null;
			while ((line = reader.readLine()) != null) {
				System.out.println(line);
			}

		} finally {

			post.releaseConnection();
			post = null;
		}

	}

	/**
	 * test update chatroom
	 * 
	 * @throws HttpException
	 * @throws IOException
	 */
	public void atestPutChatRoom() throws Exception {
		System.out.println(putChatUrl + "/mophie");
		HttpPut put = new HttpPut(putChatUrl + "/mophie");

		String authorization = new BASE64Encoder().encode("admin:admin"
				.getBytes());
		put.setHeader("authorization", authorization);
		put.setHeader("Content-Type", "application/xml;charset=UTF-8");
		// put.setHeader("Content-type",
		// "application/x-www-form-urlencoded;charset=UTF-8");
		HttpEntity entity = new StringEntity(
				"<chatRoom> <roomName>mophie</roomName> <naturalName>global-2</naturalName> <description>Global Chat Room edit</description> <password>test</password> <creationDate>2014-02-12T15:52:37.592+01:00</creationDate> <modificationDate>2014-09-12T14:20:56.286+02:00</modificationDate> <maxUsers>0</maxUsers> <persistent>true</persistent> <publicRoom>true</publicRoom> <registrationEnabled>false</registrationEnabled> "
						+ "<canAnyoneDiscoverJID>false</canAnyoneDiscoverJID> <canOccupantsChangeSubject>false</canOccupantsChangeSubject> <canOccupantsInvite>false</canOccupantsInvite> <canChangeNickname>false</canChangeNickname> "
						+ "<background>aaa://xxx.com/xx/ss/tesss.jpg</background> <membersOnly>false</membersOnly>"
						+ " <moderated>false</moderated> <broadcastPresenceRoles/> <owners> <owner>owner@localhost</owner> </owners>"
						+ " <admins> <admin>admin@localhost</admin> </admins> <members> <member>member2@localhost</member> "
						+ "<member>member1@localhost</member> </members> <outcasts> <outcast>outcast1@localhost</outcast> </outcasts>"
						+ " </chatRoom>", "utf-8");

		put.setEntity(entity);

		try {
			long time = System.currentTimeMillis();
			// 执行
			HttpResponse response = client.execute(put);
			HttpEntity entity1 = response.getEntity();
			System.out.println("----------------------------------------");
			System.out.println(response.getStatusLine());
			if (entity1 != null) {
				System.out.println("Response content length: "
						+ entity1.getContentLength());
			}
			BufferedReader reader = new BufferedReader(new InputStreamReader(
					entity1.getContent(), "UTF-8"));
			String line = null;
			while ((line = reader.readLine()) != null) {
				System.out.println(line);
			}

		} finally {

			put.releaseConnection();
			put = null;
		}

	}

	/**
	 * test delete chatroom
	 * 
	 * @throws HttpException
	 * @throws IOException
	 */
	public void atestDeleteRoom() throws Exception {

		HttpDelete delete = new HttpDelete(deletechatUrl
				+  "aaa");

		String authorization = new BASE64Encoder().encode("admin:admin"
				.getBytes());
		delete.setHeader("authorization", authorization);

		try {
			// 执行
			HttpResponse response = client.execute(delete);
			HttpEntity entity1 = response.getEntity();
			System.out.println("----------------------------------------");
			System.out.println(response.getStatusLine());
			if (entity1 != null) {
				System.out.println("Response content length: "
						+ entity1.getContentLength());
			}
			BufferedReader reader = new BufferedReader(new InputStreamReader(
					entity1.getContent(), "UTF-8"));
			String line = null;
			while ((line = reader.readLine()) != null) {
				System.out.println(line);
			}

		} finally {

			delete.releaseConnection();
			delete = null;
		}

	}
	
	/**
	 * test delete user from chatroom
	 * 
	 * @throws HttpException
	 * @throws IOException
	 */
	public void atestDeleteUser() throws Exception {

		HttpDelete delete = new HttpDelete(deletechatUrl
				+  "cc/admins/lisa");

		String authorization = new BASE64Encoder().encode("admin:111111"
				.getBytes());
		delete.setHeader("authorization", authorization);

		try {
			// 执行
			HttpResponse response = client.execute(delete);
			HttpEntity entity1 = response.getEntity();
			System.out.println("----------------------------------------");
			System.out.println(response.getStatusLine());
			if (entity1 != null) {
				System.out.println("Response content length: "
						+ entity1.getContentLength());
			}
			BufferedReader reader = new BufferedReader(new InputStreamReader(
					entity1.getContent(), "UTF-8"));
			String line = null;
			while ((line = reader.readLine()) != null) {
				System.out.println(line);
			}

		} finally {

			delete.releaseConnection();
			delete = null;
		}

	}
}
