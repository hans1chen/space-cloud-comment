package com.mophie.comment;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.Random;

import junit.framework.TestCase;

import org.apache.http.HttpEntity;
import org.apache.http.HttpException;
import org.apache.http.HttpResponse;
import org.apache.http.client.methods.HttpDelete;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.client.methods.HttpPut;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;

import sun.misc.BASE64Encoder;

public class NotificationTest extends TestCase {

	String notifyInsertURL =  "http://localhost:8080/cmservice/notify" ;
	String notifyPutURL =  "http://localhost:8080/cmservice/notify" ;
	String notifyDeleteURL =  "http://localhost:8080/cmservice/notify" ;
	CloseableHttpClient  client = null;

	protected void setUp() throws Exception {
		super.setUp();
		client = HttpClients.createDefault(); 
	}
	
	/**
	 * test post notify
	 * @throws HttpException
	 * @throws IOException
	 */
	public void atestPost() throws Exception {
		
			HttpPost  post = new HttpPost (notifyInsertURL);
			String authorization = new BASE64Encoder().encode("admin:admin".getBytes());
			post.setHeader("authorization", authorization);
			System.out.println(authorization);
			post.setHeader("Content-Type","application/xml;charset=UTF-8");
			Random rand = new Random(100023);
			HttpEntity entity = new StringEntity( "<notifications><notification><username>harryzheng</username><type>1</type><fileId>"+rand.nextInt(8)+"</fileId><comment> aa"+rand.nextInt(30)+ "</comment>"
					+ "<userId>41</userId><friendId>"+rand.nextInt(55)+"</friendId>"
					+ "</notification><notification><username>StonePD</username><type>4</type><fileId>"+rand.nextInt(30)+"</fileId><comment> aa"+rand.nextInt(30)+ "</comment>"
					+ "<userId>"+rand.nextInt(800)+"</userId><friendId>"+rand.nextInt(56)+"</friendId>"
					+ "</notification></notifications>", "utf-8"); 
			post.setEntity(entity);
		
			try {	
				// ִ��  
				HttpResponse response = client.execute(post);  
				HttpEntity entity1 = response.getEntity();  
				System.out.println("----------------------------------------");  
				System.out.println(response.getStatusLine());  
				if (entity1 != null) {  
				System.out.println("Response content length: " + entity1.getContentLength());  
				}  
				BufferedReader  reader = new BufferedReader(new InputStreamReader(entity1.getContent(), "UTF-8"));  
				String line = null;  
				while ((line = reader.readLine()) != null) {  
					System.out.println(line);  
				}  			
				  	
			} finally {

				post.releaseConnection();
				post = null;
			}
	}

	/**
	 * test put notify
	 * @throws HttpException
	 * @throws IOException
	 */
	public void atestPutNotify() throws Exception {
		
		HttpPut put =  new HttpPut (notifyPutURL+"/273/0,4");
		put.setHeader("Content-Type","application/xml;charset=UTF-8");
		String authorization = new BASE64Encoder().encode("admin:admin".getBytes());
		put.setHeader("authorization", authorization);
//		HttpEntity entity = new StringEntity( "<notification><type>1</type>"
//				+ "<userId>41</userId>"
//				+ "</notification>", "utf-8"); 
//		put.setEntity(entity);
		try {	
			// ִ��  
			HttpResponse response = client.execute(put);  
			HttpEntity entity1 = response.getEntity();  
			System.out.println("----------------------------------------");  
			System.out.println(response.getStatusLine());  
			if (entity1 != null) {  
			System.out.println("Response content length: " + entity1.getContentLength());  
			}  
			BufferedReader  reader = new BufferedReader(new InputStreamReader(entity1.getContent(), "UTF-8"));  
			String line = null;  
			while ((line = reader.readLine()) != null) {  
				System.out.println(line);  
			}  			
			  	
		} finally {

			put.releaseConnection();
			put = null;
		}
			
		
	}
	
	/**
	 * test delete notify
	 * @throws HttpException
	 * @throws IOException
	 */
	public void testDeleteNotify() throws Exception {
		
		HttpDelete put =  new HttpDelete (notifyDeleteURL+"/20a,17a");
		put.setHeader("Content-Type","application/xml;charset=UTF-8");
		String authorization = new BASE64Encoder().encode("admin:admin".getBytes());
		put.setHeader("authorization", authorization);

		try {	
			// ִ��  
			HttpResponse response = client.execute(put);  
			HttpEntity entity1 = response.getEntity();  
			System.out.println("----------------------------------------");  
			System.out.println(response.getStatusLine());  
			if (entity1 != null) {  
			System.out.println("Response content length: " + entity1.getContentLength());  
			}  
			BufferedReader  reader = new BufferedReader(new InputStreamReader(entity1.getContent(), "UTF-8"));  
			String line = null;  
			while ((line = reader.readLine()) != null) {  
				System.out.println(line);  
			}  			
			  	
		} finally {

			put.releaseConnection();
			put = null;
		}
			
		
	}
}
