package com.mophie.comment;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

import junit.framework.TestCase;

import org.apache.http.HttpEntity;
import org.apache.http.HttpException;
import org.apache.http.HttpResponse;
import org.apache.http.client.methods.HttpDelete;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;

import com.mophie.comment.common.Base64_Encode;

import sun.misc.BASE64Encoder;

public class CommentTest extends TestCase {

	String commentInsertURL =  "http://localhost:8080/cmservice/comment" ;
	String commentDeleteURL =  "http://localhost:8080/cmservice/comment" ;
	CloseableHttpClient  client = null;

	protected void setUp() throws Exception {
		super.setUp();
		client = HttpClients.createDefault(); 
	}
	
	public void testBase(){
		System.out.println(Base64_Encode.encode("ssdfd������ͷ"));
	}
	/**
	 * test post comment
	 * @throws HttpException
	 * @throws IOException
	 */
	public void atestPost() throws Exception {
		
			HttpPost  post = new HttpPost (commentInsertURL);
			String authorization = new BASE64Encoder().encode("admin:admin".getBytes());
			post.setHeader("authorization", authorization);
			post.setHeader("Content-Type","application/xml;charset=UTF-8");
//			put.setHeader("Content-type",
//					"application/x-www-form-urlencoded;charset=UTF-8");
			HttpEntity entity = new StringEntity( "<comment><fileId>22</fileId><comment>lisa aaaaaaaa</comment>"
					+ "<userId>1</userId>"
					+ "</comment>", "utf-8"); 
//			String aa = "{\"fileId\":22,\"comment\":\" dsssssaaaaaaaaaaa \",\"userId\":1} ";
//			HttpEntity entity = new StringEntity( aa, "utf-8"); 
			
			post.setEntity(entity);
		
			try {	
				// ִ��  
				HttpResponse response = client.execute(post);  
				HttpEntity entity1 = response.getEntity();  
				System.out.println("----------------------------------------");  
				System.out.println(response.getStatusLine());  
				if (entity1 != null) {  
				System.out.println("Response content length: " + entity1.getContentLength());  
				}  
				BufferedReader  reader = new BufferedReader(new InputStreamReader(entity1.getContent(), "UTF-8"));  
				String line = null;  
				while ((line = reader.readLine()) != null) {  
					System.out.println(line);  
				}  			
				  	
			} finally {

				post.releaseConnection();
				post = null;
			}
	}

	/**
	 * test delete comment
	 * @throws HttpException
	 * @throws IOException
	 */
	public void atestDelete() throws Exception {
		
		HttpDelete delete =  new HttpDelete (commentDeleteURL+"/2");
		
		String authorization = new BASE64Encoder().encode("admin:admin".getBytes());
		delete.setHeader("authorization", authorization);
		
	
		try {	
			// ִ��  
			HttpResponse response = client.execute(delete);  
			HttpEntity entity1 = response.getEntity();  
			System.out.println("----------------------------------------");  
			System.out.println(response.getStatusLine());  
			if (entity1 != null) {  
			System.out.println("Response content length: " + entity1.getContentLength());  
			}  
			BufferedReader  reader = new BufferedReader(new InputStreamReader(entity1.getContent(), "UTF-8"));  
			String line = null;  
			while ((line = reader.readLine()) != null) {  
				System.out.println(line);  
			}  			
			  	
		} finally {

			delete.releaseConnection();
			delete = null;
		}
			
		
	}
}
