package com.mophie.comment;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

import junit.framework.TestCase;

import org.apache.http.HttpEntity;
import org.apache.http.HttpException;
import org.apache.http.HttpResponse;
import org.apache.http.client.methods.HttpDelete;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;

import sun.misc.BASE64Encoder;

public class EvaluationTest extends TestCase {

	String evaluationInsertURL = "http://localhost:8080/cmservice/evaluation";
	String evaluationDeleteURL = "http://localhost:8080/cmservice/evaluation";
	CloseableHttpClient client = null;

	protected void setUp() throws Exception {
		super.setUp();
		client = HttpClients.createDefault();
	}

	/**
	 * test post evaluation
	 * 
	 * @throws HttpException
	 * @throws IOException
	 */
	public void atestPost() throws Exception {

		HttpPost post = new HttpPost(evaluationInsertURL);
		String authorization = new BASE64Encoder().encode("admin:admin"
				.getBytes());
		post.setHeader("authorization", authorization);
		post.setHeader("Content-Type", "application/xml;charset=UTF-8");
		// put.setHeader("Content-type",
		// "application/x-www-form-urlencoded;charset=UTF-8");
		HttpEntity entity = new StringEntity("<evaluation><fileId>222</fileId>"
				+ "<userId>46</userId>" + "</evaluation>", "utf-8");

		post.setEntity(entity);

		try {
			// ִ��
			HttpResponse response = client.execute(post);
			HttpEntity entity1 = response.getEntity();
			System.out.println("----------------------------------------");
			System.out.println(response.getStatusLine());
			if (entity1 != null) {
				System.out.println("Response content length: "
						+ entity1.getContentLength());
			}
			BufferedReader reader = new BufferedReader(new InputStreamReader(
					entity1.getContent(), "UTF-8"));
			String line = null;
			while ((line = reader.readLine()) != null) {
				System.out.println(line);
			}

		} finally {

			post.releaseConnection();
			post = null;
		}
	}

	/**
	 * test delete evaluation
	 * 
	 * @throws HttpException
	 * @throws IOException
	 */
	public void testDelete() throws Exception {

		HttpDelete delete = new HttpDelete(evaluationDeleteURL + "/222/2");

		String authorization = new BASE64Encoder().encode("admin:admin"
				.getBytes());
		delete.setHeader("authorization", authorization);

		try {
			// ִ��
			HttpResponse response = client.execute(delete);
			HttpEntity entity1 = response.getEntity();
			System.out.println("----------------------------------------");
			System.out.println(response.getStatusLine());
			if (entity1 != null) {
				System.out.println("Response content length: "
						+ entity1.getContentLength());
			}
			BufferedReader reader = new BufferedReader(new InputStreamReader(
					entity1.getContent(), "UTF-8"));
			String line = null;
			while ((line = reader.readLine()) != null) {
				System.out.println(line);
			}

		} finally {

			delete.releaseConnection();
			delete = null;
		}

	}
}
